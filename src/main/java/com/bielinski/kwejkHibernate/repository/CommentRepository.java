package com.bielinski.kwejkHibernate.repository;

import com.bielinski.kwejkHibernate.model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
