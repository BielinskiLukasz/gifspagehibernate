package com.bielinski.kwejkHibernate.repository;

import com.bielinski.kwejkHibernate.model.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
