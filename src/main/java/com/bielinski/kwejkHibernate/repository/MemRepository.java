package com.bielinski.kwejkHibernate.repository;

import com.bielinski.kwejkHibernate.model.Mem;
import org.springframework.data.repository.CrudRepository;

public interface MemRepository extends CrudRepository<Mem, Long> {
}
