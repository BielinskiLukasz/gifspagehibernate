package com.bielinski.kwejkHibernate.controller;

import com.bielinski.kwejkHibernate.model.Category;
import com.bielinski.kwejkHibernate.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/categories/add")
    public String add(ModelMap modelMap) {
        modelMap.addAttribute("category", new Category());
        return "categories/add";
    }

    @PostMapping("/categories")
    public String create(@ModelAttribute Category category, RedirectAttributes redirectAttributes) {
        categoryRepository.save(category);
        redirectAttributes.addFlashAttribute("message", "Category added");
        return "redirect:/categories";
    }

    @GetMapping("/categories")
    public String displayAllCategories(ModelMap modelMap) {
        modelMap.addAttribute("categories", categoryRepository.findAll());
        return "categories/index";
    }
}
