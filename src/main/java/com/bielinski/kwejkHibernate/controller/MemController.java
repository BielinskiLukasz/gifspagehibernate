package com.bielinski.kwejkHibernate.controller;

import com.bielinski.kwejkHibernate.model.Comment;
import com.bielinski.kwejkHibernate.model.Mem;
import com.bielinski.kwejkHibernate.repository.CategoryRepository;
import com.bielinski.kwejkHibernate.repository.CommentRepository;
import com.bielinski.kwejkHibernate.repository.MemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class MemController {

    @Autowired
    private MemRepository memRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CommentRepository commentRepository;

    @GetMapping("/mems/add")
    public String add(ModelMap modelMap) {
        modelMap.addAttribute("categories", categoryRepository.findAll());
        modelMap.addAttribute("mem", new Mem());
        return "mems/add";
    }

    @ResponseBody
    @PostMapping("/mems")
    public String create(@ModelAttribute Mem mem) {
        memRepository.save(mem);
        return "created";
    }

    @GetMapping("/mems")
    public String displayAllMems(ModelMap modelMap) {
        modelMap.addAttribute("mems", memRepository.findAll());
        return "mems/index";
    }

    @GetMapping("mems/{memId}")
    public String mem(@PathVariable Long memId, ModelMap modelMap) {

        Optional<Mem> memOptional = memRepository.findById(memId);
        memOptional.ifPresent(mem -> {
            modelMap.addAttribute("mem", mem);
            modelMap.addAttribute("comments", mem.getCommentList());
        });

        return "mems/show";
    }

    @PostMapping("/mems/addComment")
    public String addComment(@RequestParam String commentBody,
                             @RequestParam Long memId) {
        Optional<Mem> memOptional = memRepository.findById(memId);
        memOptional.ifPresent(
                mem -> {
                    Comment comment = new Comment();
                    comment.setCommentText(commentBody);
                    mem.addComment(comment);
                    memRepository.save(mem);
                }
        );
        return "redirect:/mems/" + memId;
    }
}
