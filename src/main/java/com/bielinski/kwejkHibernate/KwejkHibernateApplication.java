package com.bielinski.kwejkHibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KwejkHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(KwejkHibernateApplication.class, args);
	}
}
